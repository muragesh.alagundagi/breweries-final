import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreweriesService} from '../services/breweries.service';



@Component({
  selector: 'app-list-breweries',
  templateUrl: './list-breweries.component.html',
  styleUrls: ['./list-breweries.component.css']
})
export class ListBreweriesComponent implements OnInit {
  breweriesList: any;

  constructor(private breweriesService:BreweriesService,private router:Router) { }

  ngOnInit(): void {

    this.getbreweries();
  }
  getbreweries() {
    this.breweriesService.fetchBreweries().subscribe((res:any) => {
      this.breweriesList = res;
      console.log("this.breweriesList",this.breweriesList)
    })
  }

  getSingleBreweries(id:any){
    localStorage.setItem('breweries_id',id);
    this.router.navigateByUrl("/breweries-view");
  }

}
