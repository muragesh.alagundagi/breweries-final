import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreweriesViewComponent } from './breweries-view/breweries-view.component';
import { ListBreweriesComponent } from './list-breweries/list-breweries.component';

const routes: Routes = [
  {path:'', component:ListBreweriesComponent},
  {path:'breweries-view', component:BreweriesViewComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
