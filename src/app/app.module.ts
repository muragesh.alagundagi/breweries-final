import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListBreweriesComponent } from './list-breweries/list-breweries.component';
import { HttpClientModule } from "@angular/common/http";
import { BreweriesViewComponent } from './breweries-view/breweries-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBreweriesComponent,
    BreweriesViewComponent
  ],
  imports: [
    BrowserModule,      
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
