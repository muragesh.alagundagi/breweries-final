import { Component, OnInit } from '@angular/core';
import { BreweriesService} from '../services/breweries.service';


@Component({
  selector: 'app-breweries-view',
  templateUrl: './breweries-view.component.html',
  styleUrls: ['./breweries-view.component.css']
})
export class BreweriesViewComponent implements OnInit {
  breweriesDetail: any;
  id: any;
  name: any;
  phone: any;
  city: any;
  type: any;

  constructor(private breweriesService:BreweriesService) { }

  ngOnInit(): void {
    this.getSingleBrweries();
  }
  getSingleBrweries() {
    this.id = localStorage.getItem('breweries_id') 
    this.breweriesService.fetchSingleBreweries(this.id).subscribe((res:any) => {
      this.breweriesDetail = res;
      this.name  = res.name;
      this.phone = res.phone;
      this.city  = res.city;
      this.type = res.brewery_type;
      console.log("this.breweriesList",this.breweriesDetail)
    })
  }

}
