import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreweriesViewComponent } from './breweries-view.component';

describe('BreweriesViewComponent', () => {
  let component: BreweriesViewComponent;
  let fixture: ComponentFixture<BreweriesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreweriesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreweriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
