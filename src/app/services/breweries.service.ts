import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BreweriesService {

  constructor(private http:HttpClient) { }

  fetchBreweries(){  //fetch breweries list
    return this.http.get('https://api.openbrewerydb.org/breweries');
  }

  fetchSingleBreweries(id:any){  //fetch Single breweries 
    return this.http.get('https://api.openbrewerydb.org/breweries/'+id);
  }
}
